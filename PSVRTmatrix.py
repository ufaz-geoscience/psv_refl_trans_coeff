import cmath 
import numpy as np

def PSVRTmatrix(p, mi, mt):
    """
    p = ray parameter (a scalar or vector)
    mi = model of incident wave [Vp, Vs, Rho]
    mt = model of transmitted wave [Vp, Vs, Rho]

    """
    # unpack the models
    Vp1, Vs1, rho1 = mi
    Vp2, Vs2, rho2 = mt

    # vertical slownesses
    etaai = np.array([cmath.sqrt(1 / (Vp1 * Vp1) - pi * pi) for pi in p])
    etaat = np.array([cmath.sqrt(1 / (Vp2 * Vp2) - pi * pi) for pi in p])
    etabi = np.array([cmath.sqrt(1 / (Vs1 * Vs1) - pi * pi) for pi in p])
    etabt = np.array([cmath.sqrt(1 / (Vs2 * Vs2) - pi * pi) for pi in p])

    # calculate a-d coefficients
    a = rho2 * (1 - 2 * Vs2 * Vs2 * p * p) - rho1 * (1 - 2 * Vs1 * Vs1 * p * p)
    b = rho2 * (1 - 2 * Vs2 * Vs2 * p * p) + 2 * rho1 * Vs1 * Vs1 * p * p
    c = rho1 * (1 - 2 * Vs1 * Vs1 * p * p) + 2 * rho2 * Vs2 * Vs2 * p * p
    d = 2 * (rho2 * Vs2 * Vs2 - rho1 * Vs1 * Vs1)

    # calculate E, F, G, H, D coefficients
    E = b * etaai + c * etaat
    F = b * etabi + c * etabt
    G = a - d * etaai * etabt
    H = a - d * etaat * etabi
    D = E * F + G * H * p * p

    # Calculate Reflection and Transmission coefficients
    Rpp = ((b * etaai - c * etaat) * F - (a + d * etaai * etabt) * H * p * p) / D
    Rps = -1 * (2 * etaai * (a * b + d * c * etaat * etabt) * p * Vp1 / Vs1) / D
    Rss = -1 * ((b * etabi - c * etabt) * E - (a + d * etaat * etabi) * G * p * p) / D
    Rsp = -1 * (2 * etabi * (a * b + d * c * etaat * etabt) * p * (Vs1 / Vp1)) / D
    Tpp = (2 * rho1 * etaai * F * (Vp1 / Vp2)) / D
    Tps = (2 * rho1 * etaai * H * p * (Vp1 / Vs2)) / D
    Tss = 2 * rho1 * etabi * E * (Vs1 / Vs2) / D
    Tsp = -2 * (rho1 * etabi * G * p * (Vs1 / Vp2)) / D

    # stack into RTmatrix
    RTmatrix = np.column_stack((Rpp, Rps, Rss, Rsp, Tpp, Tps, Tss, Tsp))
    return RTmatrix


