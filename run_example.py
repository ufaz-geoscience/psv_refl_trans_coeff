import matplotlib.pyplot as plt
import numpy as np
from PSVRTmatrix import PSVRTmatrix

# set up the velocity model for two half-spaces
#      Vp   Vs   Density
mi = [4.98, 2.9, 2.667] # incident waves
mt = [8.00, 4.6, 3.380]  # transmitted waves

# generate the ray parameter array 
# the value goes from 0 to 1/Vp(incident)
n = 200
p = np.linspace(0, 1/mi[0], n)

# compute the coefficients
R = PSVRTmatrix(p, mi, mt)

Rpp = R[:, 0] # P-to-P reflection
Rps = R[:, 1] # P-to-S reflection
Tpp = R[:, 4] # P-to-P transmission
Tps = R[:, 5] # P-to-S transmission

# PLOTTING STARTS HERE

# incidence angle in degrees
angle = (180/np.pi) * (np.arcsin(p * mi[0]))

ymin = -0.1
ymax = 2.0
xmin = 0
xmax = 90

# reflected P
plt.subplot(position=[0.1, 0.6, 0.35, 0.3])
plt.plot(angle, np.abs(Rpp), 'k-')
plt.xlabel('Incidence angle (Degrees)')
plt.ylim(ymin, ymax)
plt.xlim(xmin, xmax)
plt.grid()
plt.title('Reflected P')

# reflected S
plt.subplot(position=[0.55, 0.6, 0.35, 0.3])
plt.plot(angle, np.abs(Rps), 'k-')
plt.xlabel('Incidence angle (Degrees)')
plt.ylim(ymin, ymax)
plt.xlim(xmin, xmax)
plt.grid()
plt.title('Reflected S')

# transmitted P
plt.subplot(position=[0.1, 0.1, 0.35, 0.3])
plt.plot(angle, np.abs(Tpp), 'k-')
plt.xlabel('Incidence angle (Degrees)')
plt.ylim(ymin, ymax)
plt.xlim(xmin, xmax)
plt.grid()
plt.title('Transmitted P')

# transmitted S
plt.subplot(position=[0.55, 0.1, 0.35, 0.3])
plt.plot(angle, np.abs(Tps), 'k-')
plt.xlabel('Incidence angle (Degrees)')
plt.ylim(ymin, ymax)
plt.xlim(xmin, xmax)
plt.grid()
plt.title('Transmitted S')

plt.show()



