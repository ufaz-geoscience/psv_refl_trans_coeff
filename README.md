# PSV_refl_trans_coeff

Python implementation of Charles Ammon's routines from here : http://eqseis.geosc.psu.edu/~cammon/HTML/UsingMATLAB/PDF/ML3%20ReflTransmission.pdf. 

"A common computation in seismology is the calculation of reflection and transmission coefficients that describe the 
partitioning of energy when a seismic wave strikes a boundary between elastic materials. The reflection coefficient 
depends on the velocities and densities on either side of the boundary and the ray parameter. The formulas for the 
values can be found in Table 3.1 of Lay and Wallace (1995) (note that an error in the sign of the second term in the 
computation of b) or in equations 5.38 and 5.39 of Aki and Richards (1980). In the welded elastic boundary, an incident 
P or SV wave results in four waves, two reflected and two transmitted or refracted waves."

"The arguments to the script include the velocity and density values for each material and the ray parameter - which can 
be a vector. That is, you can call the function with a single value of the ray parameter, or a range of ray parameters 
stored in a vector. If you pass the function a vector, it will return a complex matrix containing the R/T coefficients."

You can find a usage example in the run_example.py file.
